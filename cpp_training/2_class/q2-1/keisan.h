#ifndef _KEISAN_H_
#define _KEISAN_H_

/**
 * @class 計算クラス
 */
class Keisan
{
private:
	int m_a;	/**< 1つめの整数A */
	int m_b;	/**< 2つめの整数B */

public:

	/**
	 * set 
	 * @param a,b 計算するAとBの整数をセットする
	 */
	void set(int a, int b);

	/**
	 * add
	 * @retrun A + Bを返す
	 */
	int add();

	/**
	 * del 
	 * @retrun A - Bを返す
	 */
	int del();

	int getA();
	int getB();
};

#endif //_KEISAN_H_ 
