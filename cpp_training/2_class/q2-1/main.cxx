#include <iostream>
#include "keisan.h"

using namespace std;

int main()
{
	Keisan k;
	//k.a = 4;
	//k.b = 3;
	k.set(4, 3);
	cout << k.getA() << " + " << k.getB() << " = " << k.add() <<endl;
	cout << k.getA() << " - " << k.getB() << " = " << k.del() <<endl;
	return 0;
}
