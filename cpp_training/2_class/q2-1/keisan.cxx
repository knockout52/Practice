#include "keisan.h"

void Keisan::set(int a, int b)
{
	m_a = a;
	m_b = b;
	return;
}

int Keisan::add()
{
	return m_a + m_b;
}

int Keisan::del()
{
	return m_a - m_b;
}

int Keisan::getA()
{
	return m_a;
}

int Keisan::getB()
{
	return m_b;
}
