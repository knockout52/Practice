#include "minmax.h"

int MinMax::_max(int n1, int n2)
{
	int ret = 0;
	if (n1 > n2) {
		ret = n1;
	}
	else {
		ret = n2;
	}
	return ret;
}
	

int MinMax::max(int n1, int n2, int n3)
{
	tmp = _max(n1, n2);
	return _max(tmp, n3);
}

int MinMax::min(int n1, int n2, int n3)
{
	tmp = -(_max(-n1, -n2));
	return -(_max(-tmp, -n3));
}
