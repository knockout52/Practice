#ifndef _MINMAX_H_
#define _MINMAX_H_

class MinMax
{
private:
	int tmp;
	int _max(int n1, int n2);
public:
	int max(int n1, int n2, int n3);
	int min(int n1, int n2, int n3);
};

#endif
