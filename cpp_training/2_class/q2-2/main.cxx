#include <iostream>
#include "minmax.h"

using namespace std;

int main()
{
	MinMax m;
	cout << "4と2と7のうち、最大のものは" << m.max(4, 2, 7) <<endl;
	cout << "4と2と7のうち、最小のものは" << m.min(4, 2, 7) <<endl;
	return 0;
}
