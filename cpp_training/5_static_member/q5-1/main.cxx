#include <iostream>
#include "function.h"

using namespace std;
using namespace q5_function;

int main()
{
	int m = 3, n = 1;
    cout << m << "と" << n << "のうち最大は" << Function::max(m, n) << endl;
    cout << m << "と" << n << "のうち最小は" << Function::min(m, n) << endl;

	return 0;
}
