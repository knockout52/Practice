# 食べ物の都市毎の消費金額をグラフ表示する
#
# 取得データはBenesseのWeb APIを利用
# 参考URL
# https://www.p-pras.com/otameshi/dl_proglesson/chapter7.html


# Web APIを使うためのライブラリ
import requests

# グラフ表示用のライブラリ
from matplotlib import pyplot
from matplotlib import rcParams

# 入力ダイアログ用のライブラリ
import tkinter.ttk as ttk
import tkinter as tk

# 入力ダイアログで使う変数宣言
selectcity=''

# ボタンが押された時の処理
def click_button():
  #明示的にglobalをつけないとローカル変数になってしまう
  global selectcity
  selectcity = combo.get()
  print(selectcity)
  root.destroy()

# 品目リスト
hinmoku_dict = {
  'りんご':300,
  'みかん':301,
  'グレープフルーツ':314,
  'オレンジ':315,
  '梨':305,
  'ぶどう':306,
  '柿':307,
  '桃':308,
  'すいか':309,
  'メロン':310,
  'いちご':311,
  'バナナ':312,
  'キウイフルーツ':316,
  'ようかん':340,
  'まんじゅう':341,
  'カステラ':343,
  'ケーキ':344,
  'ゼリー':347,
  'プリン':348,
  'せんべい':350,
  'ビスケット':346,
  'スナック菓子':357,
  'キャンデー':349,
  'チョコレート':352,
  'チョコレート菓子':353,
  'アイスクリーム・シャーベット':356,
}
hinmoku_list=[]

# ダイアログボックス表示用に、品目辞書のキーの一覧を作る
for a in hinmoku_dict.keys():
  hinmoku_list.append(a)
  
# 入力用ダイアログボックスの表示
# 抜けるとselectcityに品目名が入る。
root = tk.Tk()

text = tk.Label(text="世帯当たりの年間消費金額 都市ランキングを表示します。\n\n品目を選んでください\n")
text.pack()

combo = ttk.Combobox(root, state='readonly')
combo["values"] = hinmoku_list
# デフォルトの値を食費(index=0)に設定
combo.current(0)
# コンボボックスの配置
combo.pack()

# ボタンの作成（コールバックコマンドには、コンボボックスの値を取得しprintする処理を定義）
button = tk.Button(text="選択",command=click_button)
# ボタンの配置
button.pack()

root.mainloop()


# Web APIのURLを引数に結果を呼び出す
# 品目名から辞書を引いて、品目値を指定する
url = "https://api.proglesson.com/v1/apis/cities"
params = "type="+str(hinmoku_dict[selectcity])+"&order=desc"
res = requests.get(url + '?' + params)
pyplot.rcParams["font.family"] = "MS Gothic"

# Web APIのステータスコードが正常（200）の場合
if(res.status_code == 200):
  # Web APIからの戻り値をdataに格納
  data = res.json()

  # 返ってきたデータには，
  # citiesという名前でランキングのリストが格納されている
  cities = data["cities"]
  #print(type(cities))

  # グラフ表示用と上限チェック用の変数を宣言
  citylist = []
  valuelist = []
  cnt = 1

  # MAX 30で都市名を取得する、。
  for city in cities:
    #print(city["name"], city["value"])
    citylist.insert(0, city["name"])
    valuelist.insert(0, city["value"])
    cnt += 1
    if cnt >= 30:
      break

  # 横棒グラフを表示する
  pyplot.barh(citylist, valuelist)
  pyplot.title(selectcity)
  pyplot.xlabel("世帯当たりの年間消費金額（円）")
  pyplot.show()

else:
  # Web API取得できない場合はエラー
  print("エラー")