#include <stdbool.h>

/**
 * @brief ソート用のデータを生成する
 * 
 * @param dataNum   データの個数。最大値は1000000。
 * @param array     [OUT] 生成した配列のポインタを指定する
 * @retval true     成功
 * @retval false    データ個数が1000000以上、もしくはメモリ確保失敗
 * @note  使い終わったらcleanData()関数を呼ぶこと
 */
int makeData(unsigned int dataNum, int** array);

/**
 * @brief ソート用、ソート後のデータを破棄する
 * 
 * @param array makeData()やSort関数群でソートされた配列を指定する
 */
void cleanData(int *array);