#include <stdio.h>
#include <stdlib.h>
#include "sort.h"

#define DATA_NUM 100

void printArray(int *array, int dataNum);

int main(int argc, char** argv)
{
    int *srcArray = NULL;
    int *dstBubbleSortArray = NULL;
    int *dstMergeSortArray = NULL;
    int *dstQuickSortArray = NULL;
    unsigned int count = 0;
    unsigned int time = 0;
    unsigned int dataNum = DATA_NUM;
    bool bSortDataView = false;

    if (argc >= 2) {
        dataNum = atoi(argv[1]);
    }
    if (argc >= 3) {
        bSortDataView = true;
    }
    printf(" === 元データ作成 === \n");
    makeData(dataNum, &srcArray);
    printArray(srcArray, dataNum);

    count = 0;
    time = 0;
    bubbleSort(srcArray, dataNum, &dstBubbleSortArray, &count, &time );
    if (bSortDataView == true) {
        printf(" === ソート結果 === \n");
        printArray(dstBubbleSortArray, dataNum);
    }
    printf(" === バブルソート === \n");    
    printf("データ個数 : %d 個, 比較回数 : %d 回, 処理時間 : %d マイクロ秒\n", dataNum, count, time);

    printf(" === マージソート === \n");
    count = 0;
    time = 0;
    mergeSort(srcArray, dataNum, &dstMergeSortArray, &count, &time );
    printf("データ個数 : %d 個, 比較回数 : %d 回, 処理時間 : %d マイクロ秒\n", dataNum, count, time);

    printf(" === クイックソート === \n");
    count = 0;
    time = 0;
    quickSort(srcArray, dataNum, &dstQuickSortArray, &count, &time );
    printf("データ個数 : %d 個, 比較回数 : %d 回, 処理時間 : %d マイクロ秒\n", dataNum, count, time);

    //データ削除
    cleanData(dstBubbleSortArray);
    cleanData(dstMergeSortArray);
    cleanData(dstQuickSortArray);
    cleanData(srcArray);

    return 0;
}

void printArray(int *array, int dataNum)
{
    int i = 0;
    for (i = 0; i < dataNum; i++) {
        printf("[%d] ", array[i]);
    }
    printf("\n\n");
}