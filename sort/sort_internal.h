/**
 * @brief xとyの値を入れ替える
 * 
 * @param x 
 * @param y 
 */
void swap(int *x, int *y);