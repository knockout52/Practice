#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "sort.h"
#include "sort_internal.h"

/* プロトタイプ宣言 */
void _merge_sort (int *array, int left, int right, int size, unsigned int *count);
int partition (int array[], int left, int right, unsigned int *count);
void _quick_sort (int array[], int left, int right, unsigned int *count);

/* 値を入れ替える関数 */
void swap(int *x, int *y) {
  int temp;    // 値を一時保存する変数

  temp = *x;
  *x = *y;
  *y = temp;
}

int bubbleSort(int *srcArray, unsigned int dataNum, int **dstArray, unsigned int *count, unsigned int *time )
{
    int *tmpArray = NULL;
    unsigned int i = 0, j = 0;
    struct timeval beforeTime;
    struct timeval afterTime;
    unsigned int diff_time_ms;

    tmpArray = (int *)malloc(dataNum * sizeof(int));
    if (tmpArray == NULL) {
        return false;
    }

    memcpy(tmpArray, srcArray, dataNum*sizeof(int));

    //時刻計測開始
    gettimeofday(&beforeTime, NULL);

    for (j = 0; j < dataNum; j++) {
        for (i = dataNum - 1; i > j; i--) {
            if (tmpArray[i-1] > tmpArray[i]) {
                swap(&tmpArray[i-1] , &tmpArray[i]);
            }
            (*count)++;
        }
    }
    //時刻計測終了
    gettimeofday(&afterTime, NULL);

    diff_time_ms = (afterTime.tv_sec - beforeTime.tv_sec ) * 1000000 + (afterTime.tv_usec - beforeTime.tv_usec );

    *time = diff_time_ms;
    *dstArray = tmpArray;

    return true;
}

int mergeSort(int *srcArray, unsigned int dataNum, int **dstArray, unsigned int *count, unsigned int *time ){
    int *tmpArray = NULL;
    struct timeval beforeTime;
    struct timeval afterTime;
    unsigned int diff_time_ms;

    tmpArray = (int *)malloc(dataNum * sizeof(int));
    if (tmpArray == NULL) {
        return false;
    }

    memcpy(tmpArray, srcArray, dataNum*sizeof(int));

    //時刻計測開始
    gettimeofday(&beforeTime, NULL);

    _merge_sort(tmpArray, 0, dataNum - 1, dataNum, count);

    //時刻計測終了
    gettimeofday(&afterTime, NULL);

    diff_time_ms = (afterTime.tv_sec - beforeTime.tv_sec ) * 1000000 + (afterTime.tv_usec - beforeTime.tv_usec );

    *time = diff_time_ms;
    *dstArray = tmpArray;

    return true;
}

int quickSort(int *srcArray, unsigned int dataNum, int **dstArray, unsigned int *count, unsigned int *time )
{
    int *tmpArray = NULL;
    struct timeval beforeTime;
    struct timeval afterTime;
    unsigned int diff_time_ms;

    tmpArray = (int *)malloc(dataNum * sizeof(int));
    if (tmpArray == NULL) {
        return false;
    }

    memcpy(tmpArray, srcArray, dataNum*sizeof(int));

    //時刻計測開始
    gettimeofday(&beforeTime, NULL);

    _quick_sort(tmpArray, 0, dataNum - 1, count);

    //時刻計測終了
    gettimeofday(&afterTime, NULL);

    diff_time_ms = (afterTime.tv_sec - beforeTime.tv_sec ) * 1000000 + (afterTime.tv_usec - beforeTime.tv_usec );

    *time = diff_time_ms;
    *dstArray = tmpArray;

    return true;
}

void _merge_sort (int array[], int left, int right, int size, unsigned int *count) {
  int i, j, k, mid;
  int work[size];  // 作業用配列

  if (left < right) {
    mid = (left + right)/2; // 真ん中
    _merge_sort(array, left, mid, size, count);  // 左を整列
    _merge_sort(array, mid+1, right, size, count);  // 右を整列
    for (i = mid; i >= left; i--) { work[i] = array[i]; } // 左半分
    for (j = mid+1; j <= right; j++) {
      work[right-(j-(mid+1))] = array[j]; // 右半分を逆順
    }
    i = left; j = right;
    for (k = left; k <= right; k++) {
      if (work[i] < work[j]) { array[k] = work[i++]; }
      else                   { array[k] = work[j--]; }
      (*count)++;
    }
  }
}

/***
* pivotを決め、
* 全データをpivotを境目に振り分け、
* pivotの添え字を返す
***/
int partition (int array[], int left, int right, unsigned int *count) {
  int i, j, pivot;
  i = left;
  j = right + 1;
  pivot = left;   // 先頭要素をpivotとする

  do {
    do { i++; } while (array[i] < array[pivot]);
    do { j--; } while (array[pivot] < array[j]);
    // pivotより小さいものを左へ、大きいものを右へ
    if (i < j) { 
        swap(&array[i], &array[j]); 
    }
    (*count)++;
  } while (i < j);

  swap(&array[pivot], &array[j]);   //pivotを更新

  return j;
}

/* クイックソート */
void _quick_sort (int array[], int left, int right, unsigned int *count) {
  int pivot;

  if (left < right) {
    pivot = partition(array, left, right, count);
    _quick_sort(array, left, pivot-1, count);   // pivotを境に再帰的にクイックソート
    _quick_sort(array, pivot+1, right, count);
  }
}
