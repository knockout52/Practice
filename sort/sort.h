#include <stdbool.h>
#include "data.h"


/**
 * @brief バブルソートを実行する
 * 
 * @param srcArray  ソート前の配列
 * @param dataNum   ソート前の配列の個数
 * @param dstArray  [OUT]ソート後の配列のポインタ
 * @param count     [OUT]ソート回数
 * @param time      [OUT]ソートにかかった時間
 */
int bubbleSort(int *srcArray, unsigned int dataNum, int **dstArray, unsigned int *count, unsigned int *time );

/**
 * @brief マージソートを実行する
 * 
 * @param srcArray  ソート前の配列
 * @param dataNum   ソート前の配列の個数
 * @param dstArray  [OUT]ソート後の配列のポインタ
 * @param count     [OUT]ソート回数
 * @param time      [OUT]ソートにかかった時間
 */
int mergeSort(int *srcArray, unsigned int dataNum, int **dstArray, unsigned int *count, unsigned int *time );

/**
 * @brief クイックソートを実行する
 * 
 * @param srcArray  ソート前の配列
 * @param dataNum   ソート前の配列の個数
 * @param dstArray  [OUT]ソート後の配列のポインタ
 * @param count     [OUT]ソート回数
 * @param time      [OUT]ソートにかかった時間
 */
int quickSort(int *srcArray, unsigned int dataNum, int **dstArray, unsigned int *count, unsigned int *time );