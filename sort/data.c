#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "data.h"

int makeData(unsigned int dataNum, int** array)
{
    int *tmpArray = NULL;
    unsigned int i = 0;
    
    /* 引数チェック */
    if (dataNum > 1000000) {
      return false;
    }
    srand((unsigned int)time(NULL));

    tmpArray = (int *)malloc(dataNum * sizeof(int));
    if (tmpArray == NULL) {
        return false;
    }

    for (i = 0; i < dataNum; i++) {
        tmpArray[i] = rand()%(dataNum*10);
    }

    *array = tmpArray;

    return true;
}

void cleanData(int *array)
{
    if (array != NULL) {
        free(array);
        array = NULL;
    }
    return;
}